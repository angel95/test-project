import { Component, OnInit } from '@angular/core';
import { IHNew } from '../core/beans/news';
import { HNew } from '../core/classes/news';
import { NewsService } from '../core/services/news.service';

@Component({
  selector: 'news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss']
})
export class NewsListComponent implements OnInit {
  public newsList: HNew[] = [];
  constructor(
    private newsService: NewsService
  ) { }

  ngOnInit(): void {
    this.getAllHNews();
  }

  async getAllHNews(){
    const listItems = await this.newsService.getAllHNews().toPromise();
    this.newsList = this.processHnews(listItems);
  }

  processHnews(listNews: IHNew[]){
    const arrayNews: IHNew[] = []
    listNews.forEach(hNew => {
      hNew.dateFormated = this.getFormatedDate(hNew.created_at)
      //check if story_title and title are equal to null to discard
      const isTitleNull = hNew.title === null && hNew.story_title === null;
      isTitleNull || hNew.isDeleted === 1 ? null : arrayNews.push(hNew);
    })
    return arrayNews;
  }

  openHNew(event: any, hNew: IHNew){
    event.stopPropagation();
    const url = hNew.url ? hNew.url : hNew.story_url;
    window.open(url, "_blank")
  }

  async deleteHNew(event: any, hNew: IHNew, index: number){
    event.stopPropagation();
    //logic delete from backend
    const deletedRow = await this.newsService.deleteHNew(hNew._id).toPromise();
    this.newsList.splice(index, 1);
  }

  getFormatedDate(textDate: string): string{
    //value of a day in ms
    const dayMs = (24 * 60 * 60 * 1000);
    const today = new Date();
    today.setHours(0,0,0,0)
    const dateHNew = new Date(textDate);
    const todayMs = today.getTime();
    const dateHNewMs = dateHNew.getTime();    
    //calculate the difference between the start of the date and the another date to know if it is from yesterday
    const diffTime = Math.abs(todayMs - dateHNewMs);

    if(todayMs <= dateHNewMs){
      return `${this.formatAMPM(dateHNew)}`
    } else if (diffTime < dayMs){
      return 'yesterday'
    } else {
      const month = dateHNew.toLocaleString('default', { month: 'short' }).slice(0, -1);
      return `${month} ${dateHNew.getDate()}`
    }
  }

  // transform date to 12 am-pm format
  formatAMPM(date: Date): string {
    let hours = date.getHours();
    let minutes = date.getMinutes();
    const ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    let minutesText = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutesText + ' ' + ampm;
    return strTime;
  }
}
