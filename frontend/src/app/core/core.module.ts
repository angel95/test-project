import { HttpClientModule } from '@angular/common/http';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { NewsService } from './services/news.service';

@NgModule({
  imports: [HttpClientModule],
  providers: [
    NewsService
  ]
})

export class CoreModule {
  constructor(@Optional() @SkipSelf() core: CoreModule) {
    if (core) {
        throw new Error('You shall not run!');
    }
}
}