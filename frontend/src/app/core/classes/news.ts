import { IHNew } from "../beans/news";

export class HNew implements IHNew {
  constructor(
    public _id: string,
    public externalId: number,
    public author: string,
    public created_at: string,
    public isDeleted: number,
    public story_title: string,
    public story_url: string,
    public title: string,
    public url: string,
    public dateFormated?: string
  ) {}

}