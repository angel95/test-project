export interface IHNew {
  _id: string;
  externalId: number;
  author: string;
  created_at: string;
  isDeleted: number;
  story_title: string;
  story_url: string;
  title: string,
  url: string,
  dateFormated?: string;
}