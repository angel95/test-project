import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IHNew } from '../beans/news';
import { HNew } from '../classes/news';


@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(
    private http: HttpClient,
  ) { }

  getAllHNews(): Observable<any> {
    const URL = `http://localhost:3000/news`;
    return this.http.get(URL, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        })
    }); 
  }

  deleteHNew(id: string): Observable<any> {
    const URL = `http://localhost:3000/news/${id}`;
    return this.http.post(URL, {}, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        })
    }); 
  }
}
