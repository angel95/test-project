
import { HttpModule } from '@nestjs/axios';
import { Module, Global } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { AxiosRequestConfig } from 'axios';

import config from '../config';

const API_KEY = '12345634';
const API_KEY_PROD = 'PROD1212121SA';
@Global()
@Module({
  imports: [
    MongooseModule.forRootAsync({
      useFactory: async (configService: ConfigType<typeof config>) => {
        const { connection, username, password, host, port, name, authSource, readPreference } = configService.mongo;
        return {
          uri: `${connection}://mongo:${port}`,
          user: username,
          pass: password,
          dbName: name,
          authSource: authSource,
          readPreference: readPreference,
          directConnection: true,
          useNewUrlParser: true
        }
      },
      inject: [config.KEY],
    }),
    HttpModule.registerAsync({
      useFactory: async (configService: ConfigType<typeof config>): Promise<AxiosRequestConfig> => {
        const timeout = 5000;
        const maxRedirects = 5
        return { timeout, maxRedirects };
      },
    })
  ],
  providers: [
    {
      provide: 'API_KEY',
      useValue: process.env.NODE_ENV === 'prod' ? API_KEY_PROD : API_KEY,
    },
  ],
  exports: ['API_KEY', MongooseModule],
})
export class DatabaseModule { }
