import { registerAs } from '@nestjs/config';

export default registerAs('config', () => {
  return {
    mongo: { 
      // #     PORT: 3000
      // #     MONGO_INITDB_ROOT_USERNAME: root
      // #     MONGO_INITDB_ROOT_PASSWORD: root
      // #     MONGO_DB: reign-store
      // #     MONGO_PORT: 27017
      // #     MONGO_HOST: localhost
      // #     MONGO_CONNECTION: mongodb
      // #     MONGO_AUTHSOURCE: admin
      // #     MONGO_READ_PREFERENCE: primary
      // name: process.env.MONGO_DB
      // username: process.env.MONGO_INITDB_ROOT_USERNAME,
      // password: process.env.MONGO_INITDB_ROOT_PASSWORD,
      // port: parseInt(process.env.MONGO_PORT, 10),
      // host: process.env.MONGO_HOST,
      // connection: process.env.MONGO_CONNECTION,
      // authSource: process.env.MONGO_AUTHSOURCE,
      // readPreference: process.env.MONGO_READ_PREFERENCE

      name: 'reign-store',
      username: 'root',
      password: 'root',
      port: 27017,
      host: 'localhost',
      connection: 'mongodb',
      authSource: 'admin',
      readPreference: 'primary'
    }
  };
});