import { IsDate, IsString, IsUrl } from "class-validator";


export class CreateNewsDto {
  @IsString()
  readonly title: string;

  @IsString()
  readonly story_title: string;

  @IsUrl()
  readonly url: string;

  @IsUrl()
  readonly story_url: string;

  @IsDate()
  readonly created_at: Date;

  @IsString()
  readonly author: string;
  
}

export class UpdateNewsDto extends CreateNewsDto {};