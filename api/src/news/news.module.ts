import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { NewsController } from './controllers/news.controller';
import { News, NewsSchema } from './entities/news.entity';
import { NewsService } from './services/news.service';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([
      {
        name: News.name,
        schema: NewsSchema
      }
    ])
  ], 
  controllers: [NewsController],
  providers: [NewsService]
})
export class NewsModule {}

