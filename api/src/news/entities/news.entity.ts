import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";


@Schema()
export class News extends Document {
  @Prop()
  title: string;

  @Prop()
  story_title: string;

  @Prop()
  url: string;

  @Prop()
  story_url: string;

  @Prop({required: true})
  created_at: string;

  @Prop({required: true})
  author: string;

  @Prop({requierd: true, unique: true})
  externalId: number;

  //1 true, 0 false
  @Prop({default: 0})
  isDeleted: number;
}

export const NewsSchema = SchemaFactory.createForClass(News)