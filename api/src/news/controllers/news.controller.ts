import { Controller, Get, Param, Post } from "@nestjs/common";
import { NewsService } from "../services/news.service";

@Controller('news')
export class NewsController {
  constructor(private newsService: NewsService){

  }

  @Get()
  getAllNews(){
    return this.newsService.getAllNewsFromDataBase();
  }

  @Get('/forcedNews')
  forcedGetNews(){
    return this.newsService.getAllNewsAndSaveIt();
  }

  //Logic delete from the notice
  @Post('/:id')
  deletStateNews(@Param('id') id: string){
    return this.newsService.logicDeleteNews(id);
  }
}