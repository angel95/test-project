import { HttpService } from "@nestjs/axios";
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Cron, CronExpression } from "@nestjs/schedule";
import { Model, QueryOptions, UpdateQuery } from "mongoose";
import { concatMap, from, map, retryWhen, toArray } from "rxjs";
import { News } from "../entities/news.entity";


@Injectable()
export class NewsService {
  constructor(@InjectModel(News.name) private NewsModel: Model<News>,
  private httpService: HttpService,
  ){}

  @Cron(CronExpression.EVERY_HOUR)
  async cronTogetAllNews() {
    await this.getAllNewsAndSaveIt();
  }

  async getAllNewsFromAlgolia(){
    return await this.httpService.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs').pipe(map(response => response.data.hits)).toPromise();
  }


  async getAllNewsAndSaveIt(){
    let news = await this.getAllNewsFromAlgolia();
    const newsSaved = await from(news).pipe(
      concatMap(async (oneNew: any) => {
        oneNew['externalId'] = oneNew.objectID;
        return await this.NewsModel.findOneAndUpdate({externalId: oneNew.externalId}, {...oneNew}, {upsert: true, new: true})
      }), toArray()
    ).toPromise();
    return newsSaved
  }

  async getAllNewsFromDataBase(){
    return this.NewsModel.find().exec();
  }

  async logicDeleteNews(id: string){
    await this.NewsModel.findByIdAndUpdate(id, {isDeleted: 1})
    return this.NewsModel.findById(id)
  }
}